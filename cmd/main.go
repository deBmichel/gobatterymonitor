package main

import (
	"batterychecker/gui"
	"batterychecker/monitor"
	f "fmt"
	"time"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		panic("Error imposible set env")
	}

	batteryStatus := monitor.GetAMonitorInstance()

	err = batteryStatus.ObtainBateryPercentage()
	if err != nil {
		panic("Error obtaining Batery percentage")
	}

	action, batterylevel := batteryStatus.DetermineAction()
	if batterylevel != -1 {
		gui.LaunchStatusGUI(action, batterylevel)
	} else {
		f.Println(batteryStatus)
	}

	f.Println("Battery Monitor running at", time.Now())
}
