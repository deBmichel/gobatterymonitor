module batterychecker/cmd

go 1.17

replace batterychecker/monitor => ../monitor

replace batterychecker/gui => ../gui

require (
	batterychecker/gui v0.0.0-00010101000000-000000000000
	batterychecker/monitor v0.0.0-00010101000000-000000000000
	github.com/joho/godotenv v1.4.0
)
