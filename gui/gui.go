package gui

import (
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strconv"
)

func LaunchStatusGUI(action string, batterylevel int) {
	body := []byte(prepareHtml(action, batterylevel))
	err := ioutil.WriteFile(os.Getenv("HTMLANSNAME"), body, 0644)
	if err != nil {
		log.Fatal("Error creating html file", err)
	}

	command := os.Getenv("OPENBROWSERWITHANS")

	_, err = exec.Command("/bin/sh", "-c", command).Output()
	if err != nil {
		println("Serious error executing:", command)
		panic(err)
	}
}

func prepareHtml(chargerOperation string, batterylevel int) string {
	batteryImg := ""

	switch {
	case chargerOperation == "Connect the charger":
		batteryImg = "rsrcs/baterylow.png"
	case chargerOperation == "Disconnect the charger":
		batteryImg = "rsrcs/bateryhigh.png"
	default:
		log.Println("batery Image is white: ", chargerOperation, batterylevel)
		batteryImg = "rsrcs/notfound.html"
	}

	return `<!DOCTYPE html
	<html>
		<head>
		    <script type="text/javascript">
		    function CustomAlert() {
		        var audio = new Audio('rsrcs/alarm.mp3');
		        audio.play();
		    }
		    </script>
		    <style type="text/css">
		    	#imggo { border: 5px solid #00BFFF; }
		        #status{ color: #00BFFF; }
		        h1, h6 { color:white; }
		    </style>
		</head>

		<body bgcolor="black" onload="CustomAlert()">
		    <div style="text-align: center;">
		        <a href="">
		            <img id="imggo" src="rsrcs/gopherimg.png" height="100vh" width="100vh">
		        </a>
		        <h6>powered by Golang</h6>
		        <h1>Reviewing Batery Status:</h1>

		        <h3 id="status">` + chargerOperation + `</h3>
		        <a href="">
		            <img id="imggo" src="` + batteryImg + `" height="200vh" width="100vh">
		        </a>
		        <h2 id="status">` + strconv.Itoa(batterylevel) + `%</h3>
		    </div>
		    </div>
		</body>
	</html>`
}
