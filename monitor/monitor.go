package monitor

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
)

type Status struct {
	State      string
	Percentage int
}

func GetAMonitorInstance() *Status {
	return &Status{}
}

func (state *Status) ObtainBateryPercentage() (err error) {
	command := os.Getenv("BATERYSTATUSCOMMAND")
	stateIndicator := os.Getenv("BATERYSTATUSINDICATOR")
	percentageIndicator := os.Getenv("BATERYPERCENTAGEINDICATOR")

	out, err := exec.Command("/bin/sh", "-c", command).Output()
	if err != nil {
		println("Serious error executing:", command)
		panic(err)
	}

	scanner := bufio.NewScanner(strings.NewReader(string(out)))
	for scanner.Scan() {
		line := strings.ReplaceAll(scanner.Text(), " ", "")
		switch {
		case strings.Contains(line, stateIndicator):
			state.State = strings.Split(line, ":")[1]
		case strings.Contains(line, percentageIndicator):
			re := regexp.MustCompile("[0-9]+")
			strPercentage := re.FindAllString(line, 1)
			state.Percentage, _ = strconv.Atoi(strPercentage[0])
		}
	}

	return err
}

func (state *Status) DetermineAction() (string, int) {
	minimumChargeLevel, _ := strconv.Atoi(os.Getenv("BATERYMINIMUMLEVEL"))
	maximumChargeLevel, _ := strconv.Atoi(os.Getenv("BATERYMAXIMUMLEVEL"))
	chargingIndicator := os.Getenv("CHARGINGMESSAGE")
	dischargingIndicator := os.Getenv("DISCHARGINGMESSAGE")

	fmt.Println("min:", minimumChargeLevel, "max:", maximumChargeLevel)

	switch {
	case state.Percentage < minimumChargeLevel && state.State == dischargingIndicator:
		return "Connect the charger", state.Percentage
	case state.Percentage > maximumChargeLevel && state.State == chargingIndicator:
		return "Disconnect the charger", state.Percentage
	default:
		return "", -1
	}
}
